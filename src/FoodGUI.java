import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JButton secondMenu;
    private JButton firstMenu;
    private JButton thirdMenu;
    private JButton fifthMenu;
    private JButton sixthMenu;
    private JTextPane totalOrder;
    private JLabel totalPrice;
    private JPanel root;
    private JButton checkOut;
    private JLabel firstMenuName;
    private JLabel secondMenuName;
    private JLabel thirdMenuName;
    private JLabel fourthMenuName;
    private JLabel fifthMenuName;
    private JLabel sixthMenuName;
    private JButton fourthMenu;
    private int totalFoodPrice = 0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setContentPane(new FoodGUI().root);
    }

    void orderFood(String foodName, int price) {
        int firstConfirmation = JOptionPane.showConfirmDialog(null,
                "Would like to order " + foodName + "?",
                "Order Confirmation", JOptionPane.YES_NO_OPTION);
        if (firstConfirmation == 0) {
            int secondConfirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like the large size (+50yen) ?",
                    "Choose size", JOptionPane.YES_NO_OPTION);
            if(secondConfirmation == 0){
                JOptionPane.showMessageDialog(null,
                        "Thank you for ordering " + foodName + "(large size)! It will be served as soon as possible.");
                String currentText = totalOrder.getText();
                totalOrder.setText(currentText + foodName + "(large size) " + (price+50) + "yen\n");
                totalFoodPrice += price+50;
                totalPrice.setText("Total " + totalFoodPrice + " yen");
            }
            else if(secondConfirmation == 1){
                JOptionPane.showMessageDialog(null,
                        "Thank you for ordering " + foodName + "(medium size)! It will be served as soon as possible.");
                String currentText = totalOrder.getText();
                totalOrder.setText(currentText + foodName + " " + price + "yen\n");
                totalFoodPrice += price;
                totalPrice.setText("Total " + totalFoodPrice + " yen");
            }
        }
    }

    public FoodGUI() {
        totalPrice.setText("Total " + totalFoodPrice + " yen\n");

        firstMenu.setIcon(new ImageIcon(this.getClass().getResource("menu1.jpg")));
        firstMenuName.setText("Beef bowl 100 yen");
        firstMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Beef bowl", 100);
            }
        });

        secondMenu.setIcon(new ImageIcon(this.getClass().getResource("menu2.jpg")));
        secondMenuName.setText("Curry Rice 200 yen");
        secondMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Curry Rice", 200);
            }
        });

        thirdMenu.setIcon(new ImageIcon(this.getClass().getResource("menu3.jpg")));
        thirdMenuName.setText("Minced Tuna Bowl 300 yen");
        thirdMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Minced Tuna Bowl", 300);
            }
        });

        fourthMenu.setIcon(new ImageIcon(this.getClass().getResource("menu4.jpg")));
        fourthMenuName.setText("Pork Rice Bowl 400 yen");
        fourthMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Pork Rice Bowl", 400);
            }
        });
        fifthMenu.setIcon(new ImageIcon(this.getClass().getResource("menu5.jpg")));
        fifthMenuName.setText("Chili Shrimp Rice Bowl 500 yen");
        fifthMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Chili Shrimp Rice Bowl", 500);
            }
        });

        sixthMenu.setIcon(new ImageIcon(this.getClass().getResource("menu6.jpg")));
        sixthMenuName.setText("Minced Chicken Bowl 600 yen");
        sixthMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Minced Chicken Bowl", 600);
            }
        });

        checkOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would like to check out?",
                        "Check out Confirmation", JOptionPane.YES_NO_OPTION);
                if(totalFoodPrice!=0) {
                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(null,
                                "Thank you. The total price is " + totalFoodPrice + " yen.");
                        totalOrder.setText("");
                        totalFoodPrice = 0 ;
                        totalPrice.setText("Total " + totalFoodPrice + " yen");
                    }
                }
            }
        });
    }
}






